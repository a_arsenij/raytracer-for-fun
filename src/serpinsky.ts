import {createTriangle, Triangle, XYZ} from "./trace";

function mid(
    a: XYZ,
    b: XYZ
): XYZ {
    return [
        (a[0] + b[0]) / 2,
        (a[1] + b[1]) / 2,
        (a[2] + b[2]) / 2,
    ]
}

export function serpinsky(
    a: XYZ,
    b: XYZ,
    c: XYZ,
    d: XYZ,
    depth: number = 4,
    addTo: Triangle[] = [],
): Triangle[] {
    if (depth === 0) {
        addTo.push(
            createTriangle(a, b, c),
            createTriangle(c, b, d),
            createTriangle(d, b, a),
            //createTriangle(a, d, c),
        )
    } else {
        const e = mid(a, b);
        const f = mid(b, c);
        const g = mid(a, c);
        const h = mid(b, d);
        const i = mid(a, d);
        const j = mid(c, d);

        function sub(a: XYZ, b: XYZ, c: XYZ, d: XYZ) {
            serpinsky(a, b, c, d, depth - 1, addTo);
        }

        sub(b, e, f, h);
        sub(e, a, g, i);
        sub(f, g, j, c);
        sub(h, i, d, j);
    }
    return addTo;
}
