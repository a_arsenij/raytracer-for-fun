import {RGB} from "./image";

export type Matter = {
    albedo: RGB,
    luminosity: RGB,
    roughness: number,
}

export type XYZ = [number, number, number]

export type Triangle = {
    kind: "triangle",
    vertices: [
        XYZ,
        XYZ,
        XYZ,
    ]
}

export function createTriangle(
    a: XYZ,
    b: XYZ,
    c: XYZ
): Triangle {
    return {
        kind: "triangle",
        vertices: [a, b, c]
    }
}

export type Sphere = {
    kind: "sphere",
    center: XYZ,
    radius: number,
}

export function createSphere(
    center: XYZ,
    radius: number
): Sphere {
    return {
        kind: "sphere",
        center: center,
        radius: radius
    }
}

export type Shape = Triangle | Sphere

export type Obstacle = Matter & Shape

export type Ray = {
    isInitial: boolean,
    source: XYZ,
    direction: XYZ,
}

export type Intersection = {
    at: XYZ,
    normal: XYZ,
}

type N3 = [number, number, number];

function slau3(
    a: N3,
    b: N3,
    c: N3,
    d: N3,
): N3 | undefined {

    /*
     * [Linear equations]
     * { i * a[0] + j * b[0] + k * c[0] + d[0] = 0
     * { i * a[1] + j * b[1] + k * c[1] + d[1] = 0
     * { i * a[2] + j * b[2] + k * c[2] + d[2] = 0
     */

    function swapItems(arr: N3, i1: number, i2: number) {
        const tmp = arr[i1]!;
        arr[i1] = arr[i2]!;
        arr[i2] = tmp;
    }

    const arrs = [a, b, c, d];

    function swap(i1: number, i2: number) {
        arrs.forEach(arr => swapItems(arr, i1, i2));
    }

    let skipFirst = false;
    if (a[0] === 0) {
        if (a[1] !== 0) {
            swap(0, 1);
        } else if (a[2] !== 0) {
            swap(0, 2);
        } else {
            skipFirst = true;
        }
    }

    const a1a0 = skipFirst ? 1 : a[1] / a[0];
    const a2a0 = skipFirst ? 1 : a[2] / a[0];

    const e: [number, number] = [
        b[1] - b[0] * a1a0,
        b[2] - b[0] * a2a0
    ];
    const f: [number, number] = [
        c[1] - c[0] * a1a0,
        c[2] - c[0] * a2a0
    ]
    const g: [number, number] = [
        d[1] - d[0] * a1a0,
        d[2] - d[0] * a2a0
    ]
    const e1e0 = e[1] / e[0]

    const k = (
        g[0] * e1e0 - g[1]
    ) / (
        f[1] - f[0] * e1e0
    );

    const j = (k * f[0] + g[0]) / (-e[0])

    const i = (j * b[0] + k * c[0] + d[0]) / (-a[0])

    return [i, j, k];

}


function negate(
    vec: XYZ,
): XYZ {
    return [
        -vec[0],
        -vec[1],
        -vec[2],
    ]
}

function vecFromTo(
    from: XYZ,
    to: XYZ
): XYZ {
    return [
        to[0] - from[0],
        to[1] - from[1],
        to[2] - from[2],
    ]
}

function crossProduct(
    b: XYZ,
    c: XYZ
): XYZ {
    return [
        b[1] * c[2] - c[1] * b[2],
        b[0] * c[2] - c[0] * b[2],
        b[0] * c[1] - c[0] * b[1],
    ]
}

function getTriangleIntersection(
    triangle: Triangle,
    ray: Ray,
): Intersection | undefined {
    /*
     *                 E
     *        B_      /
     *       /  \_   /
     *      /     \_/
     *     /       /\_
     *    /       F   \_
     *   /       .      \_
     *  /       .         \_
     * A--------------------C
     *        /
     *       /
     *      D
     *
     * [Triangle plane]
     * P = A + i * AB + j * AC
     * [Ray]
     * R = D + k * DE
     * [Intersection]
     * P = R
     *
     * [1]
     * A + i * AB + j * AC = D + k * DE
     *
     * [2]
     * { A[0] + i * AB[0] + j * AC[0] = D[0] + k * DE[0]
     * { A[1] + i * AB[1] + j * AC[1] = D[1] + k * DE[1]
     * { A[2] + i * AB[2] + j * AC[2] = D[2] + k * DE[2]
     *
     * [3]
     * { i * AB[0] + j * AC[0] + k * ED[0] + A[0] - D[0] = 0
     * { i * AB[1] + j * AC[1] + k * ED[1] + A[1] - D[1] = 0
     * { i * AB[2] + j * AC[2] + k * ED[2] + A[2] - D[2] = 0
     *
     * [Linear equations]
     * { i * a[0] + j * b[0] + k * c[0] + d[0] = 0
     * { i * a[1] + j * b[1] + k * c[1] + d[1] = 0
     * { i * a[2] + j * b[2] + k * c[2] + d[2] = 0
     *
     */

    const A = triangle.vertices[0];
    const B = triangle.vertices[1];
    const C = triangle.vertices[2];
    const D = ray.source;
    const AB = vecFromTo(A, B);
    const AC = vecFromTo(A, C);
    const ED = negate(ray.direction)

    const a: N3 = [
        AB[0],
        AB[1],
        AB[2]
    ]
    const b: N3 = [
        AC[0],
        AC[1],
        AC[2]
    ]
    const c: N3 = [
        ED[0],
        ED[1],
        ED[2]
    ]
    const d: N3 = [
        A[0] - D[0],
        A[1] - D[1],
        A[2] - D[2],
    ]

    const solution = slau3(a, b, c, d);
    if (solution === undefined) {
        return undefined;
    }

    const [i, j, k] = solution;

    const isInsideOfTriangle = i >= 0 &&
        j >= 0 && (i + j) <= 1


    if (!isInsideOfTriangle) {
        return undefined;
    }

    const x = D[0] + k * ray.direction[0];
    const y = D[1] + k * ray.direction[1];
    const z = D[2] + k * ray.direction[2];

    const pt: XYZ = [x, y, z];

    let maxDir = 0;
    let maxValue = 0;
    for (let i = 0; i < 3; i++) {
        const v = Math.abs(ray.direction[i]!);
        if (v > maxValue) {
            maxValue = v;
            maxDir = i;
        }
    }

    maxValue = ray.direction[maxDir]!;
    if (maxValue > 0 && ray.source[maxDir]! > pt[maxDir]!) {
        return undefined;
    }
    if (maxValue < 0 && ray.source[maxDir]! < pt[maxDir]!) {
        return undefined;
    }

    const cross = normalize(crossProduct(
        AB,
        AC
    ));
    const dot = dotProduct(cross, ray.direction);
    const neg = dot > 0;
    return {
        at: pt,
        normal: neg ? negate(cross) : cross
    }


}

function getSphereIntersection(
    sphere: Sphere,
    ray: Ray,
): Intersection | undefined {

    /*
     * RAY:
     * { x = ray.source[0] + ray.direction[0] * k
     * { y = ray.source[1] + ray.direction[1] * k
     * { z = ray.source[2] + ray.direction[2] * k
     *
     * SPHERE:
     * Math.sqrt(
     *     (x - sphere.center[0]) ** 2 +
     *     (y - sphere.center[1]) ** 2 +
     *     (z - sphere.center[2]) ** 2
     * ) === sphere.radius
     *
     * [1]
     * Math.sqrt(
     *     ((ray.source[0] + ray.direction[0] * k) - sphere.center[0]) ** 2 +
     *     ((ray.source[1] + ray.direction[1] * k) - sphere.center[1]) ** 2 +
     *     ((ray.source[2] + ray.direction[2] * k) - sphere.center[2]) ** 2
     * ) === sphere.radius
     *
     * [2]
     * (
     *     + (ray.source[0] + ray.direction[0] * k) ** 2
     *     - 2 * (ray.source[0] + ray.direction[0] * k) * sphere.center[0]
     *     + sphere.center[0] ** 2
     *
     *     + (ray.source[1] + ray.direction[1] * k) ** 2
     *     - 2 * (ray.source[1] + ray.direction[1] * k) * sphere.center[1]
     *     + sphere.center[1] ** 2
     *
     *     + (ray.source[2] + ray.direction[2] * k) ** 2
     *     - 2 * (ray.source[2] + ray.direction[2] * k) * sphere.center[2]
     *     + sphere.center[2] ** 2
     * ) - sphere.radius ** 2 === 0
     *
     * [3]
     * (
     *     + (ray.source[0] + ray.direction[0] * k) ** 2
     *     + (ray.source[1] + ray.direction[1] * k) ** 2
     *     + (ray.source[2] + ray.direction[2] * k) ** 2
     *
     *     - 2 * ray.direction[0] * k * sphere.center[0]
     *     - 2 * ray.direction[1] * k * sphere.center[1]
     *     - 2 * ray.direction[2] * k * sphere.center[2]
     *
     *     - 2 * ray.source[0] * sphere.center[0]
     *     - 2 * ray.source[1] * sphere.center[1]
     *     - 2 * ray.source[2] * sphere.center[2]
     *
     *     + sphere.center[0] ** 2
     *     + sphere.center[1] ** 2
     *     + sphere.center[2] ** 2
     * ) - sphere.radius ** 2 === 0
     *
     * [4]
     * (
     *     + (ray.source[0] + ray.direction[0] * k) ** 2
     *     + (ray.source[1] + ray.direction[1] * k) ** 2
     *     + (ray.source[2] + ray.direction[2] * k) ** 2
     *
     *     - 2 * ray.direction[0] * k * sphere.center[0]
     *     - 2 * ray.direction[1] * k * sphere.center[1]
     *     - 2 * ray.direction[2] * k * sphere.center[2]
     * ) - (
     *     + sphere.radius ** 2
     *     + 2 * ray.source[0] * sphere.center[0]
     *     + 2 * ray.source[1] * sphere.center[1]
     *     + 2 * ray.source[2] * sphere.center[2]
     *     - sphere.center[0] ** 2
     *     - sphere.center[1] ** 2
     *     - sphere.center[2] ** 2
     * ) === 0
     *
     * [5]
     * (
     *     + ray.direction[0] ** 2 * k ** 2
     *     + ray.direction[1] ** 2 * k ** 2
     *     + ray.direction[2] ** 2 * k ** 2
     *     + 2 * ray.source[0] * ray.direction[0] * k
     *     + 2 * ray.source[1] * ray.direction[1] * k
     *     + 2 * ray.source[2] * ray.direction[2] * k
     *     - 2 * ray.direction[0] * k * sphere.center[0]
     *     - 2 * ray.direction[1] * k * sphere.center[1]
     *     - 2 * ray.direction[2] * k * sphere.center[2]
     * ) - (
     *     - ray.source[0] ** 2
     *     - ray.source[1] ** 2
     *     - ray.source[2] ** 2
     *     + sphere.radius ** 2
     *     + 2 * ray.source[0] * sphere.center[0]
     *     + 2 * ray.source[1] * sphere.center[1]
     *     + 2 * ray.source[2] * sphere.center[2]
     *     - sphere.center[0] ** 2
     *     - sphere.center[1] ** 2
     *     - sphere.center[2] ** 2
     * ) === 0
     *
     * [6]
     * (
     *     + (
     *            + ray.direction[0] ** 2
     *            + ray.direction[1] ** 2
     *            + ray.direction[2] ** 2
     *     ) * k ** 2
     *     + 2 * (
     *            + ray.source[0] * ray.direction[0]
     *            + ray.source[1] * ray.direction[1]
     *            + ray.source[2] * ray.direction[2]
     *            - ray.direction[0] * sphere.center[0]
     *            - ray.direction[1] * sphere.center[1]
     *            - ray.direction[2] * sphere.center[2]
     *     ) * k
     * ) - (
     *     - ray.source[0] ** 2
     *     - ray.source[1] ** 2
     *     - ray.source[2] ** 2
     *     + sphere.radius ** 2
     *     + 2 * ray.source[0] * sphere.center[0]
     *     + 2 * ray.source[1] * sphere.center[1]
     *     + 2 * ray.source[2] * sphere.center[2]
     *     - sphere.center[0] ** 2
     *     - sphere.center[1] ** 2
     *     - sphere.center[2] ** 2
     * ) === 0
     *
     * [7] Square equation:
     * a * x ** 2 + b * x + c = 0
     * a = (
     *            + ray.direction[0] ** 2
     *            + ray.direction[1] ** 2
     *            + ray.direction[2] ** 2
     *     )
     * b = 2 * (
     *            + ray.source[0] * ray.direction[0]
     *            + ray.source[1] * ray.direction[1]
     *            + ray.source[2] * ray.direction[2]
     *            - ray.direction[0] * sphere.center[0]
     *            - ray.direction[1] * sphere.center[1]
     *            - ray.direction[2] * sphere.center[2]
     * )
     * c = - (
     *     - ray.source[0] ** 2
     *     - ray.source[1] ** 2
     *     - ray.source[2] ** 2
     *     + sphere.radius ** 2
     *     + 2 * ray.source[0] * sphere.center[0]
     *     + 2 * ray.source[1] * sphere.center[1]
     *     + 2 * ray.source[2] * sphere.center[2]
     *     - sphere.center[0] ** 2
     *     - sphere.center[1] ** 2
     *     - sphere.center[2] ** 2
     * )
     *
     */

    const a = (
        ray.direction[0] ** 2
        + ray.direction[1] ** 2
        + ray.direction[2] ** 2
    )

    const b = 2 * (
        ray.source[0] * ray.direction[0]
        + ray.source[1] * ray.direction[1]
        + ray.source[2] * ray.direction[2]
        - ray.direction[0] * sphere.center[0]
        - ray.direction[1] * sphere.center[1]
        - ray.direction[2] * sphere.center[2]
    )

    const c = -(
        0
        - ray.source[0] ** 2
        - ray.source[1] ** 2
        - ray.source[2] ** 2
        + sphere.radius ** 2
        + 2 * ray.source[0] * sphere.center[0]
        + 2 * ray.source[1] * sphere.center[1]
        + 2 * ray.source[2] * sphere.center[2]
        - sphere.center[0] ** 2
        - sphere.center[1] ** 2
        - sphere.center[2] ** 2
    )

    const d = b * b - 4 * a * c;
    if (d < 0) {
        return undefined;
    }
    const ds = Math.sqrt(d);
    const k1 = (-b - ds) / 2 / a;
    const k2 = (-b + ds) / 2 / a;

    const x1 = ray.source[0] + ray.direction[0] * k1
    const y1 = ray.source[1] + ray.direction[1] * k1
    const z1 = ray.source[2] + ray.direction[2] * k1

    const x2 = ray.source[0] + ray.direction[0] * k2
    const y2 = ray.source[1] + ray.direction[1] * k2
    const z2 = ray.source[2] + ray.direction[2] * k2

    const p1: XYZ = [x1, y1, z1];
    const p2: XYZ = [x2, y2, z2];

    let maxValueAxis = 0;
    let maxValue = 0;
    for (let i = 1; i < 3; i++) {
        const a = Math.abs(ray.direction[i]!);
        if (a > maxValue) {
            maxValue = a;
            maxValueAxis = i;
        }
    }

    const pts: XYZ[] = [p1, p2, ray.source];
    pts.sort((a, b) => a[maxValueAxis]! - b[maxValueAxis]!);
    let srcIndex = pts.findIndex(e => e === ray.source);

    let pt: XYZ;
    let insideFactor = -1;
    if (srcIndex === 1) {
        // inside
        insideFactor = 1;
        if (ray.direction[maxValueAxis]! > 0) {
            pt = pts[2]!;
        } else {
            pt = pts[0]!;
        }
    } else {
        // outside
        pt = pts[1]!;
        if (
            ray.direction[maxValueAxis]! > 0 &&
            pt[maxValueAxis]! < ray.source[maxValueAxis]!
        ) {
            return undefined;
        }
        if (
            ray.direction[maxValueAxis]! < 0 &&
            pt[maxValueAxis]! > ray.source[maxValueAxis]!
        ) {
            return undefined;
        }
    }

    const normal = normalize([
        insideFactor * (sphere.center[0] - pt[0]),
        insideFactor * (sphere.center[1] - pt[1]),
        insideFactor * (sphere.center[2] - pt[2]),
    ])

    return {
        at: pt,
        normal: normal,
    }

}

function createTriangleAABB(
    triangle: Triangle
): AABB {
    return {
        a: [
            Math.min(triangle.vertices[0][0], triangle.vertices[1][0], triangle.vertices[2][0],),
            Math.min(triangle.vertices[0][1], triangle.vertices[1][1], triangle.vertices[2][1],),
            Math.min(triangle.vertices[0][2], triangle.vertices[1][2], triangle.vertices[2][2],),
        ],
        b: [
            Math.max(triangle.vertices[0][0], triangle.vertices[1][0], triangle.vertices[2][0],),
            Math.max(triangle.vertices[0][1], triangle.vertices[1][1], triangle.vertices[2][1],),
            Math.max(triangle.vertices[0][2], triangle.vertices[1][2], triangle.vertices[2][2],),
        ],
    }
}

function createSphereAABB(
    sphere: Sphere
): AABB {
    return {
        a: [
            sphere.center[0] - sphere.radius,
            sphere.center[1] - sphere.radius,
            sphere.center[2] - sphere.radius,
        ],
        b: [
            sphere.center[0] + sphere.radius,
            sphere.center[1] + sphere.radius,
            sphere.center[2] + sphere.radius,
        ],
    };
}

const preparedAABBs = new WeakMap<Obstacle, AABB>();

function getAABB(
    obstacle: Obstacle,
): AABB {
    return getCached(
        preparedAABBs,
        obstacle,
        (k) => {
            switch (k.kind) {
                case "triangle":
                    return createTriangleAABB(k);
                case "sphere":
                    return createSphereAABB(k);
            }
        }
    )
}

function getCached<K extends object, V>(
    cache: WeakMap<K, V>,
    key: K,
    fun: (k: K) => V,
): V {
    let old = cache.get(key);
    if (old !== undefined) {
        return old;
    }
    old = fun(key);
    cache.set(key, old);
    return old;
}

export function getIntersection(
    obstacle: Obstacle,
    ray: Ray
): Intersection | undefined {
    const aabb = getAABB(obstacle);
    if (!doesAABBIntersects(aabb, ray)) {
        return undefined;
    }
    switch (obstacle.kind) {
        case "sphere":
            return getSphereIntersection(obstacle, ray);
        case "triangle":
            return getTriangleIntersection(obstacle, ray);
    }
}

type Scene = Obstacle[];

function getVecLength(
    vec: XYZ
): number {
    return Math.sqrt(
        vec[0] * vec[0] +
        vec[1] * vec[1] +
        vec[2] * vec[2]
    );
}

export function normalize(
    vec: XYZ
): XYZ {
    const l = getVecLength(vec);
    return [
        vec[0] / l,
        vec[1] / l,
        vec[2] / l,
    ]
}

type AABB = {
    a: XYZ,
    b: XYZ,
}

function doesAABBIntersects(
    aabb: AABB,
    ray: Ray
): boolean {

    /*
     * RAY:
     * { x = ray.source[0] + ray.direction[0] * k
     * { y = ray.source[1] + ray.direction[1] * k
     * { z = ray.source[2] + ray.direction[2] * k
     *
     * AABB:
     * { x >= aabb.a[0] && x <= aabb.b[0]
     * { y >= aabb.a[1] && y <= aabb.b[1]
     * { z >= aabb.a[2] && z <= aabb.b[2]
     *
     * [1]
     * { ray.source[0] + ray.direction[0] * k >= aabb.a[0]
     * { ray.source[0] + ray.direction[0] * k <= aabb.b[0]
     *
     * [2]
     * { ray.direction[0] * k >= aabb.a[0] - ray.source[0]
     * { ray.direction[0] * k <= aabb.b[0] - ray.source[0]
     *
     * [3]
     * { k >= (aabb.a[0] - ray.source[0]) / ray.direction[0]
     * { k <= (aabb.b[0] - ray.source[0]) / ray.direction[0]
     *
     * [4]
     * { kMax = (aabb.a[0] - ray.source[0]) / ray.direction[0]
     * { kMin = (aabb.b[0] - ray.source[0]) / ray.direction[0]
     */


    let kMin = Number.NEGATIVE_INFINITY;
    let kMax = Number.POSITIVE_INFINITY;

    for (let i = 0; i < 3; i++) {
        if (ray.direction[i] !== 0) {
            const a = (aabb.a[i]! - ray.source[i]!) / ray.direction[i]!;
            const b = (aabb.b[i]! - ray.source[i]!) / ray.direction[i]!;
            const min = Math.min(a, b);
            const max = Math.max(a, b);
            kMin = Math.max(kMin, min);
            kMax = Math.min(kMax, max);
        }
    }

    if (kMin > kMax) {
        return false;
    }

    const kAvg = (kMin + kMax) / 2;

    const x = ray.source[0] + ray.direction[0] * kAvg
    const y = ray.source[1] + ray.direction[1] * kAvg
    const z = ray.source[2] + ray.direction[2] * kAvg

    const inX = aabb.a[0] <= x && x <= aabb.b[0]
    const inY = aabb.a[1] <= y && y <= aabb.b[1]
    const inZ = aabb.a[2] <= z && z <= aabb.b[2];

    return inX && inY && inZ;
}

function getSquaredDistance(
    a: XYZ,
    b: XYZ
) {
    return a.map((aa, idx) => (aa - b[idx]!) ** 2).reduce((a, b) => a + b)
}

function getNearestIntersection(
    scene: Scene,
    ray: Ray,
): [Obstacle, Intersection] | undefined {

    let nearestObstacle: Obstacle | undefined = undefined;
    let nearestIntersection: Intersection | undefined = undefined;
    let smallestDistance: number = Number.POSITIVE_INFINITY;
    const EPSILON = 0.0000001;

    for (let o of scene) {
        const inter = getIntersection(o, ray);
        if (inter !== undefined) {
            const distance = getSquaredDistance(ray.source, inter.at);
            if (distance < smallestDistance && distance > EPSILON) {
                smallestDistance = distance;
                nearestObstacle = o;
                nearestIntersection = inter;
            }
        }
    }

    if (smallestDistance < Number.POSITIVE_INFINITY) {
        return [nearestObstacle!, nearestIntersection!]
    } else {
        return undefined;
    }

}

function getReflectionVector(
    reflecting: XYZ,
    normal: XYZ
): XYZ {
    const k = dotProduct(normal, reflecting);
    return [
        reflecting[0] + normal[0] * k * -2.0,
        reflecting[1] + normal[1] * k * -2.0,
        reflecting[2] + normal[2] * k * -2.0,
    ];
}

function dotProduct(
    a: XYZ,
    b: XYZ
) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

const raysThatFacedNothing = new WeakSet<Ray>();

const BLACK: RGB = [0, 0, 0];

function traceRay(
    scene: Scene,
    ray: Ray,
    maxReflections: number,
): RGB {

    if (ray.isInitial && raysThatFacedNothing.has(ray)) {
        return BLACK;
    }

    const inter = getNearestIntersection(
        scene,
        ray,
    );
    if (inter === undefined) {
        if (ray.isInitial) {
            raysThatFacedNothing.add(ray);
        }
        return BLACK;
    }
    const [obstacle, intersection] = inter;

    const cos = Math.max(0, -dotProduct(
        ray.direction,
        inter[1].normal
    ));
    const cosedLuminosity = obstacle.luminosity.map(v =>
        v * cos
    ) as XYZ;

    if (maxReflections === 0) {
        return cosedLuminosity;
    }

    let refVector = getReflectionVector(
        ray.direction,
        intersection.normal
    );

    if (obstacle.roughness > 0) {
        const s = Math.pow(obstacle.roughness * 1.3, 2.5);
        refVector[0] += (Math.random() - 0.5) * s;
        refVector[1] += (Math.random() - 0.5) * s;
        refVector[2] += (Math.random() - 0.5) * s;
    }
    refVector = normalize(refVector);

    const other = traceRay(
        scene,
        {
            source: intersection.at,
            direction: refVector,
            isInitial: false,
        },
        maxReflections - 1,
    );

    const albedo = obstacle.albedo

    return [
        other[0] * albedo[0] + cosedLuminosity[0],
        other[1] * albedo[1] + cosedLuminosity[1],
        other[2] * albedo[2] + cosedLuminosity[2],
    ];


}

export async function traceScene(
    scene: Scene,
    initialRays: Ray[],
    samplesPerPixel: number,
    maxReflections: number,
    onStepDone: (samplesDone: number, colors: RGB[]) => Promise<void>,
) {

    const initialRaysNormalized = initialRays.map(v => ({
        isInitial: true,
        source: v.source,
        direction: normalize(v.direction)
    }));

    const samples: RGB[] = [];
    for (let ray = 0; ray < initialRaysNormalized.length; ray++) {
        samples.push([0, 0, 0]);
    }

    for (let sample = 0; sample < samplesPerPixel; sample++) {
        const a = new Date().getTime();
        for (let rayIdx = 0; rayIdx < initialRaysNormalized.length; rayIdx++) {
            const ray = initialRaysNormalized[rayIdx]!;
            const rgb = traceRay(
                scene,
                ray,
                maxReflections,
            );
            for (let c = 0; c < 3; c++) {
                samples[rayIdx]![c] += rgb[c]!;
            }
        }
        const b = new Date().getTime();
        console.log("Took " + (b - a) + "ms")
        await onStepDone(
            sample + 1,
            samples.map(v => v.map(vv => vv / (sample + 1)) as RGB)
        );

    }

}
