import {clamp} from "./utils";

export type RGB = [number, number, number];

export type Image = {
    readonly width: number,
    readonly height: number,
    readonly setColor: (x: number, y: number, color: RGB) => void,
    readonly getColor: (x: number, y: number) => RGB,
}

export function createImage(
    width: number,
    height: number,
    defaultColor: RGB
): Image {
    const backingArray: RGB[] = [];
    for (let i = 0; i < width * height; i++) {
        backingArray.push(defaultColor);
    }
    const xy = (x: number, y: number) => {
        const xx = clamp(0, x, width - 1);
        const yy = clamp(0, y, height - 1);
        return xx + yy * width;
    }
    return {
        width, height,
        getColor: (x, y) => {
            const res = backingArray[xy(x, y)];
            if (!res) {
                throw new Error("??");
            }
            return res;
        },
        setColor: (x, y, color) => {
            backingArray[xy(x, y)] = color;
        }
    }
}
