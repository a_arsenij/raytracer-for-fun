import {createImage, RGB} from "./image";
import {saveImage} from "./jimp";
import {createTriangle, normalize, Obstacle, traceScene} from "./trace";
import {serpinsky} from "./serpinsky";

const w = 1024;
const h = 1024;

const image = createImage(
    w, h,
    [0, 0, 0]
);

const COUNT = 4
const A = 0.8
const scene: Obstacle[] = serpinsky(
    [512, 30, 1000],
    [30, 996, 1000],
    [996, 996, 1000],
    [512, 996, 2000],
    3
).map((s) => ({
    ...s,
    albedo: [A, A, A] as RGB,
    luminosity: [
        0, 0, 0
    ] as RGB,
    roughness: 0.6,
}));

scene.push({
    ...createTriangle(
        [-1000000, -1000000, -100],
        [2000000, -1000000, -100],
        [512, 2000000, -100]
    ),
    albedo: [1, 1, 1] as RGB,
    luminosity: [
        1, 1, 1
    ] as RGB,
    roughness: 0,
})
scene.reverse();

/*const scene: Obstacle[] = [...Array(COUNT)].map((_, x) =>
    [...Array(COUNT)].map((_, y) => {
        const l = Math.pow(x / (COUNT - 1), 2);
        const r = y / (COUNT - 1);
        return ({
            ...createSphere([
                130 + x * 256,
                130 + y * 256,
                1000
            ], 100),
            albedo: [1, 1, 1] as RGB,
            luminosity: [
                l * Math.random(),
                l * Math.random(),
                l * Math.random()
            ] as RGB,
            roughness: Math.sqrt(r),
        })
    })).flat()*/

traceScene(
    scene,
    [...Array(w * h)].map((_, idx) => {

        const x = Math.floor(idx / w);
        const y = idx % w;
        const dx = (x * 2 - w);
        const dy = (y * 2 - h);
        const K = 1
        const direction = normalize([dx * K, dy * K, 1000])
        return {
            source: [0, h / 2, 0],
            direction: direction,
            isInitial: true,
        }
    }),
    100,
    6,
    async (samples, rgbs) => {
        rgbs.forEach((rgb, idx) => {
            const x = Math.floor(idx / w);
            const y = idx % w;
            image.setColor(
                x, y,
                rgb
            )
        });
        const filename = "generated/res" + samples + ".png"
        await saveImage(image, filename);
        console.log("Saved as " + filename);
    }
)

