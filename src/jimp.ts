import Jimp from "jimp";
import {Image} from "./image";
import {clamp} from "./utils";

export function saveImage(
    image: Image,
    fileName: string
) {
    let img = new Jimp(
        image.width,
        image.height,
        (err, image) => {
            if (err) throw err;
        }
    );

    for (let x = 0; x < image.width; x++) {
        for (let y = 0; y < image.height; y++) {
            const color = image.getColor(x, y);
            img.setPixelColor(
                Jimp.rgbaToInt(
                    clamp(0, color[0] * 255, 255),
                    clamp(0, color[1] * 255, 255),
                    clamp(0, color[2] * 255, 255),
                    255
                ),
                x,
                y
            );
        }
    }

    return new Promise((res) => {
        img.write(fileName, () => {
            res(undefined);
        });
    })
}
