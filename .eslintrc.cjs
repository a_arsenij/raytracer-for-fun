module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "standard"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module",
        "project": "./tsconfig.json"
    },
    "plugins": [
        "@typescript-eslint",
        "simple-import-sort"
    ],
    "rules": {
        "quotes": [
            "error",
            "double"
        ],
        "max-len": ["error", {
            "code": 100,
            "ignoreTemplateLiterals": true,
            "ignorePattern": "^\\} from \".*\";$"
        }],
        "no-tabs": [
            "error", {
                "allowIndentationTabs": false
            }
        ],
        "indent": [
            "error", 4, {
                "ignoreComments": false,
                "SwitchCase": 1,
                "MemberExpression": 1
            }
        ],
        "semi": ["error", "always"],
        "comma-dangle": [
            "error", {
                "arrays": "always-multiline",
                "objects": "always-multiline",
                "imports": "always-multiline",
                "exports": "always-multiline",
                "functions": "always-multiline"
            }
        ],
        "prefer-template": ["error"],
        "simple-import-sort/imports": "error",
        "object-curly-spacing": ["error", "always"],
        "curly": ["error", "all"],
        "keyword-spacing": ["error", {
            "before": true,
            "after": true
        }],
        "arrow-parens": ["error", "always"],
        "arrow-spacing": ["error", {
            "before": true,
            "after": true
        }],
        "brace-style": ["error", "1tbs"],
        "no-useless-constructor": "off",
        "@typescript-eslint/space-before-blocks": ["error", "always"],
        "@typescript-eslint/switch-exhaustiveness-check": "error",
        "@typescript-eslint/member-delimiter-style": [
            "error", {
                "multiline": {
                    "delimiter": "comma",
                    "requireLast": true
                }
            }
        ],
        "@typescript-eslint/type-annotation-spacing": ["error", {
            "before": false,
            "after": true,
            "overrides": {
                "arrow": {
                    "before": true
                }
            }
        }],
        "no-use-before-define": "off",
        "space-before-function-paren": ["error", "never"],
        "array-callback-return": "off",
        "no-unused-vars": ["warn"],
    }
}
